"""splendor game"""
import random
import uuid
import csv
from collections import defaultdict
import time


class TooManyTokens(Exception):
    """exception for too many tokens"""
    pass


class NotEnoughMoney(Exception):
    """exception for not enough money"""
    pass


class Noble(object):
    """class to store noble tiles"""

    def __init__(self, cost):
        """cost is a dict of cost color to number"""
        self.cost = cost
        self.points = 3

    def get_points(self):
        """return number of points for noble tile"""
        return self.points

    def check_cost(self, card_list):
        """check if cost of noble tile has been satisfied"""
        card_list_value = defaultdict(int)
        for card in card_list:
            card_list_value[card.get_discount()] += 1
        for key in self.cost:
            if card_list_value[key] < self.cost[key]:
                return False
        return True


class Card(object):
    """class to store card info"""

    def __init__(self, discount, tokens, level):
        self.points = int(discount[1])
        self.discount = discount[0]
        self.tokens = tokens
        self._id = uuid.uuid4().get_hex()
        self.level = level

    def __hash__(self):
        return hash(self._id)

    def get_level(self):
        """get the level of the card"""
        return self.level

    def get_discount(self):
        """get discount from card"""
        return self.discount

    def get_points(self):
        """get points from card"""
        return self.points

    def get_id(self):
        """get the card id"""
        return self._id

    def __repr__(self):
        return "<Card %d, %d, %s, %s>" % (self.level, self.points, self.discount, self.tokens)


class Player(object):
    """class to store player info"""

    def __init__(self, name, timelimit):
        self.name = name
        self.tokens = defaultdict(int)
        self.cards = []
        self.reserved = []
        self.nobles = []
        self._id = uuid.uuid4().get_hex()
        self.time_left = timelimit

    def add_tokens(self, token_list):
        """method to add tokens to a player"""
        for token in token_list:
            self.tokens[token] += 1
        if self.total_tokens() > 10:
            raise TooManyTokens

    def get_total_points(self):
        """get total points for player"""
        points = 0
        for card in self.cards:
            points += card.get_points()
        for noble in self.nobles:
            points += noble.get_points()
        return points

    def total_tokens(self):
        """method to return total number of tokens a player has"""
        return sum(v for v in self.tokens.values())

    def take_turn(self, card, token_list, reserve):
        """take a turn for a player"""
        if token_list is not None:
            try:
                self.add_tokens(token_list)
            except TooManyTokens:
                print "too many tokens, fix later"
        if reserve and card is not None:
            self.reserved.append(card)
        elif card is not None:
            self.cards.append(card)

    def __repr__(self):
        return "<Player %s: tokens[%s] cards[%s] reserved[%s] time[%d]>" % (
            self.name, self.tokens, self.cards, self.reserved, self.time_left)


class Game(object):
    """class to store game state"""

    def __init__(self, numplayers, timelimit):
        self.players = [Player(str(n), timelimit) for n in xrange(numplayers)]
        self.tokens = {}
        self.time_keeper = int(time.time())
        for color in ["white", "blue", "green", "red", "black"]:
            self.tokens[color] = 7
        self.tokens["gold"] = 5
        self.cards = {
            1: [],
            2: [],
            3: [],
        }
        self.board_cards = {
            1: [],
            2: [],
            3: [],
        }
        with open('./cards.csv', 'r') as csv_file:
            cardreader = csv.reader(csv_file, delimiter=',')
            for row in cardreader:
                self.cards[int(row[0])].append(Card((row[1], row[2]), tuple(row[3:]), int(row[0])))

    def shuffle(self):
        """shuffle the cards in the deck"""
        for key in self.cards:
            random.shuffle(self.cards[key])

    def deal_card(self, level):
        """deal one card"""
        card = self.cards[level].pop()
        self.board_cards[level].append(card)

    def deal_cards_to_start(self):
        """deal 12 cards to start the game"""
        for _ in xrange(4):
            for i in xrange(1, 4):
                self.deal_card(i)

    def find_card_by_id(self, card_id):
        """finds card on board by id and removes it"""
        for card in self.board_cards[1] + self.board_cards[2] + self.board_cards[3]:
            if card.get_id() == card_id:
                return card

    def take_card_from_board(self, card):
        """take a card from the board"""
        self.board_cards[card.get_level()].remove(card)
        self.deal_card(card.get_level())

    def take_card_from_deck(self, deck_level):
        """remove card from deck and return"""
        if len(self.cards[deck_level]) > 0:
            return self.cards[deck_level].pop()
        else:
            return None

    def start_game(self):
        """starts game by dealing cards"""
        self.shuffle()
        self.deal_cards_to_start()
        random.shuffle(self.players)
        self.time_keeper = int(time.time())
        print self.tokens
        print self.board_cards
        print self.players

    def take_tokens(self, token_list):
        """remove tokens from board for player"""
        if len(token_list) > 3:
            raise Exception("Can only take 3 tokens")
        if len(token_list) > 2 and len(set(token_list)) == 1:
            raise Exception("Can only take two of same token")
        if len(token_list) == 2 and len(set(token_list)) == 1:
            if self.tokens[token_list[0]] < 4:
                raise Exception("Cannot take two tokens when less than 4")
        for token in token_list:
            self.tokens[token] -= 1

    def add_tokens(self, token_list):
        """add tokens back to board"""
        for token in token_list:
            self.tokens[token] += 1

    def next_turn(self, card_id=None, token_list=None, reserve=False, deck_level=0):
        """initiate the next turn
        card_id: card id to take from board
        token_list: list of tokens to choose
        reserve: True = reserve, False = not reserve
        deck_level: choose card from deck_level (0 default)
        """
        player = self.players.pop()
        if card_id is not None and deck_level > 0:
            raise Exception("Invalid Turn, Choose Card or Deck")
        if card_id is not None and token_list is not None:
            raise Exception("Invalid Turn, Choose Card or Tokens")
        if token_list is not None and deck_level > 0:
            raise Exception("Invalid Turn, Choose Deck or Tokens")
        if deck_level > 0 and not reserve:
            raise Exception("Invalid Turn, Can only reserve cards from Deck")
        if card_id is not None or deck_level > 0 and reserve:
            if player.get_reserve_card_num() == 3:
                raise Exception("Invalid Turn, Too Many reserved Cards")
        if card_id is not None:
            card = self.find_card_by_id(card_id)
        elif deck_level > 0:
            card = self.take_card_from_deck(deck_level)
        else:
            card = None
        if token_list is not None:
            self.take_tokens(token_list)
        valid_turn = True
        try:
            return_tokens = player.take_turn(card, token_list, reserve)
        except NotEnoughMoney:
            valid_turn = False
        self.add_tokens(return_tokens)
        if valid_turn:
            self.take_card_from_board(card)
        self.players = [player] + self.players


if __name__ == "__main__":
    G = Game(4, 5*60)
    G.start_game()
